from house_expense import *
import shelve

# TODO: 1. Taking advance payments
#       2. Implementing the bazar

def init_people() -> dict:
    people_names = ['Nihal', 'Farzad', 'Sabir',
                    'Rifaz', 'Linkon', 'Roman']
    people = {}
    for name in people_names:
        people[name] = HouseMate(name)
    return people

def take_adv_payment(hm: HouseMate, amount: float, reserve: Reserve):
    reserve.take_payment(Payment(hm, amount))
    hm.current_cash += amount
    hm.total_payment_made += amount

def take_adv_payment_for_all(people: dict, reserve: Reserve):
    for buddy in people.values():
        amount = float(input(
            'Adv payment amount for %s: ' % (buddy.name)
        ))
        take_adv_payment(buddy, amount, reserve)
    print('Taken adv payment for all. :)')

def scan_items_and_evaluate(people: dict) -> None:
    """Scan the 'items_bought.txt' file that contains name of items space
    separated by their price. Then evaluate the accounts of each House Mate """

    # These people pay for eggs in the house
    egg_people = ['Linkon', 'Roman', 'Rifaz', 'Sabir']

    # These people pay for daal, oil, onion, garlic and salt
    daal_people = ['Linkon', 'Rifaz', 'Roman', 'Farzad', 'Nihal', 'Sabir']
    daal_items = ['daal', 'oil', 'garlic', 'onion', 'salt']

    # Any food except eggs and daal_items
    rice_people = ['Linkon', 'Rifaz', 'Roman', 'Sabir']
    rice_items = ['rice', 'bhaat', 'fish', 'meat']

    all_people = ['Linkon', 'Rifaz', 'Roman', 'Farzad', 'Sabir', 'Nihal']
    # house_people = all_people.copy.append('Sourav')

    # items_list = {group, people_array}
    items_list = {
            'daal': daal_people,
            'rice': rice_people,
            'eggs': egg_people,
    }

    items_file = open('items_bought.txt', 'r').read().split('\n')

    # sanitization for trailing empty indices
    for _ in range(items_file.count('')):
        items_file.remove('')

    for row in items_file:
        [item, price] = row.split()

        if item in daal_items:
            group = 'daal'
        elif item in rice_items:
            group = 'rice'
        else:
            group = 'eggs'

        print('\nItem:', item)
        for consumer_name in items_list[group]:
            num_of_consumers = len(items_list[group])
            price_to_pay = int(price) / num_of_consumers
            consumer = people[consumer_name]
            consumer.current_cash -= price_to_pay
            print(consumer.name+ ' currently has: ' + str(consumer.current_cash))
            #print(consumer.name + ' should pay: ' + str(price_to_pay))
        print('Total consumers:', num_of_consumers)


def main():
    with shelve.open('he-data') as db:
        try:
            people = db['people']
        except:
            people = init_people()
            print('no people in db')

        try:
            reserve = db['reserve']
        except:
            reserve = Reserve('Linkon')
            reserve.init_payments()
            print('no reserve in db')

    # ask user for input
    user_choice = input(
        "1/a: Take advance payments\n2/b: Scan list of items: "
    )

    if user_choice in '1a':
        name = input("Name of HouseMate, or type 'all': ")
        if name == 'all':
            take_adv_payment_for_all(people, reserve)
        else:
            amount = float(input('Amount: '))
            hm = people[name]
            take_adv_payment(hm, amount, reserve)
    elif user_choice in '2b':
        scan_items_and_evaluate(people)
    else:
        for person in people.values():
            print(person.name, person.current_cash)

    with shelve.open('he-data') as db:
            db['people'] = people 
            db['reserve'] = reserve

if __name__ == "__main__":
    main()

