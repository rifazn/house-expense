import datetime

class HouseMate:
    def __init__(self, name, current_cash = 0.0):
        self.name = name
        self.current_cash = current_cash
        self.total_payment_made = 0.0

    def __repr__(self) -> str:
        return self.name

    def __eq__(self, other) -> bool:
        return self.name == other.name

    def __hash__(self) -> int:
        return hash(self.name)

class Payment:
    def __init__(self, payment_madeby: HouseMate, amount: float, date = datetime.date.today()):
        self.by = payment_madeby
        self.amt = amount
        self.date = date

class Reserve:
    def __init__(self, name: str):
        self.name = name
        self.available_cash = 0.0
        self.received_cash = 0.0
        self.brought_forward_cash = 0.0
        self.next_payment_date = (
                datetime.date.today() + datetime.timedelta(days=7)
        )
        self.payment_amt = {} # stores the amount a HouseMate should pay
        self.payments_made = {}
        self.weekly_payments_list = []
        # self.amt_left = {}

    def init_payments(self, people) -> None:
        """Set the weekly payment amount of each HouseMate"""
        p_amt = self.payment_amt # 'p_amt' so I don't have to write it fully

        p_amt['Nihal'] = 100.00
        p_amt['Farzad'] = 100.00
        p_amt['Sabir'] = 100.00
        p_amt['Rifaz'] = 200.00
        p_amt['Linkon'] = 200.00
        p_amt['Roman'] = 200.00

        for p in people.values():
            self.payments_made[p] = 0.0

    def take_payment(self, payment: Payment) -> None:
        """Let the Reserve receive a preferably weekly payment."""
        self.available_cash += payment.amt
        self.received_cash += payment.amt
        self.payments_made[payment.by] += payment.amt

        print('\n\ntake payment hoise naki thik moto\n')
        for buddy, amt in self.payments_made.items():
            print(buddy, amt)

    def who_paid_excess(self, people: 'HouseMates') -> list:
        """Find who have paid in advance"""
        paid_excess = {} # List of those who have paid in excess

        for buddy_name, amt in self.payment_amt.items():
            buddy = people[buddy_name]
            amt_paid = self.payments_made.setdefault(buddy, 0)
            print(type(amt_paid), type(amt))

            if amt_paid > amt:
                paid_excess[buddy] = amt_paid - amt

        return paid_excess
    
    def who_are_left(self, people) -> None:
        """Find who haven't paid the full amount yet."""
        left = {} # list of who hasn't paid how much

        for buddy_name, amt in self.payment_amt.items():
            buddy = people[buddy_name]
            amt_paid = self.payments_made.setdefault(buddy, 0)
            if amt_paid < amt:
                left[buddy] = amt - amt_paid

        return left

    def week_expired(self) -> bool:
        today = datetime.date.today()
        return today >= self.next_payment_date

    def go_to_next_week(self) -> None:
        today = datetime.date.today()
        self.next_payment_date = today + datetime.timedelta(7)
        self.weekly_payments_list.append(self.payments_made)
        self.payments_made = {}


