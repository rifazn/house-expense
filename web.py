from flask import Flask, render_template, request, redirect
import shelve
from helpers import *

web_app = Flask(__name__)

@web_app.route('/')
def index() -> 'html':
    people, reserve = get_people_and_reserve()
    dues = reserve.who_are_left(people)
    if not reserve.week_expired():
        return render_template('who_are_left.html', persons=dues)
    else:
        for person in people.values():
            person.current_cash -= dues.get(person, 0)
        reserve.go_to_next_week()
        update_db(people, reserve) # Because the next payment date was updated
        return process_who_are_left()


@web_app.route('/advance')
def take_advances() -> 'html':
    """Take advance payments of the HouseMates"""
    return render_template('advance.html',
        the_title="HouseExpense Helper - Advance payments",
        housemates = ['Linkon', 'Roman', 'Nihal',
                      'Rifaz', 'Farzad', 'Sabir'])

@web_app.route('/take_advance', methods=['POST'])
def take_advance() -> 'html':
    people, reserve = get_people_and_reserve()

    if request.form.get('Nihal'):
        amount = int(request.form['Nihal'])
        take_adv_payment(people['Nihal'], amount, reserve)

    if request.form.get('Rifaz'):
        amount = int(request.form['Rifaz'])
        take_adv_payment(people['Rifaz'], amount, reserve)

    if request.form.get('Linkon'):
        amount = int(request.form['Linkon'])
        take_adv_payment(people['Linkon'], amount, reserve)

    if request.form.get('Roman'):
        amount = int(request.form['Roman'])
        take_adv_payment(people['Roman'], amount, reserve)

    if request.form.get('Farzad'):
        amount = int(request.form['Farzad'])
        take_adv_payment(people['Farzad'], amount, reserve)

    if request.form.get('Sabir'):
        amount = int(request.form['Sabir'])
        take_adv_payment(people['Sabir'], amount, reserve)
    
    update_db(people, reserve)
    return current_cash()

@web_app.route('/currents')
def current_cash():
    people, reserve = get_people_and_reserve()
    return render_template('current_accounts.html',
            reserve_name="Reserve",
            reserve_cash=reserve.available_cash,
            the_accounts=people)

@web_app.route('/bazar')
def bazar() -> 'html':
    return render_template('/bazar.html')

@web_app.route('/bazar_eval', methods=['POST'])
def bazar_eval() -> 'html':
    items_list = request.form['bazarlist']
    items = items_list.split('\r\n')
    #items.remove('')
    people, reserve = get_people_and_reserve()
    print(items)
    scan_items_and_evaluate(people, reserve, items)
    update_db(people, reserve)
    for p in people.values():
        print(p.name, p.current_cash)
    return render_template('current_accounts.html', the_accounts=people)

@web_app.route('/who_are_left')
def process_who_are_left() -> 'html':
    people, reserve = get_people_and_reserve()
    pers = reserve.who_are_left(people)
    return render_template('who_are_left.html', persons=pers)

@web_app.route('/who_paid_excess')
def process_who_paid_excess() -> 'html':
    people, reserve = get_people_and_reserve()
    pers = reserve.who_paid_excess(people)
    return render_template('who_paid_excess.html', persons=pers)

if __name__ == "__main__":
    web_app.run(debug=True)
