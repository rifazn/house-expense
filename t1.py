from app import *

with shelve.open('tester-data') as db:
    try:
        people = db['people']
    except:
        people = init_people()
        print('no people in db')

    try:
        reserve = db['reserve']
    except:
        reserve = Reserve('Linkon')
        reserve.init_payments()
        print('no reserve in db')

take_adv_payment_for_all(people, reserve)
print("Checking each people's accounts after taking advance.")
for p in people.values():
    print(p.name, p.current_cash)

scan_items_and_evaluate(people)

print("Check acconts after buying stuff.")
for p in people.values():
    print(p.name, p.current_cash)
with shelve.open('tester-data') as db:
        db['people'] = people 
        db['reserve'] = reserve
