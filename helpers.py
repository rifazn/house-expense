from house_expense import *
import shelve

def init_people() -> dict:
    people_names = ['Nihal', 'Farzad', 'Sabir',
                    'Rifaz', 'Linkon', 'Roman']
    people = {}
    for name in people_names:
        people[name] = HouseMate(name)
    return people

def get_people_and_reserve() -> tuple:
    with shelve.open('web-data') as db:
        try:
            people = db['people']
        except:
            print('no people in db')
            people = init_people()

        try:
            reserve = db['reserve']
        except:
            print('no reserve in db')
            reserve = Reserve('Linkon')
            reserve.init_payments(people)
    return (people, reserve, )

def update_db(people: 'list of HouseMates', reserve: Reserve) -> None:
    with shelve.open('web-data') as db:
            db['people'] = people 
            db['reserve'] = reserve

def everyones_current_cash(people: list) -> list:
    ll = []
    for p in people.values():
        ll.append((p.name, p.current_cash))
    return ll

def take_adv_payment(hm: HouseMate, amount: float, reserve: Reserve):
    reserve.take_payment(Payment(hm, amount))
    hm.current_cash += amount
    hm.total_payment_made += amount

def take_adv_payment_for_all(people: dict, reserve: Reserve):
    for buddy in people.values():
        amount = float(input(
            'Adv payment amount for %s: ' % (buddy.name)
        ))
        take_adv_payment(buddy, amount, reserve)
    print('Taken adv payment for all. :)')

def scan_items_and_evaluate(people: dict, r: Reserve, items: list) -> None:
    """Scan the 'items_bought.txt' file that contains name of items space
    separated by their price. Then evaluate the accounts of each House Mate 
    Questions: What if someone buys tomato separately?"""

    # These people pay for eggs in the house
    egg_people = ['Linkon', 'Roman', 'Rifaz', 'Sabir']

    # These people pay for daal, oil, onion, garlic and salt
    daal_people = ['Linkon', 'Rifaz', 'Roman', 'Farzad', 'Nihal', 'Sabir']
    daal_items = ['daal', 'oil', 'garlic', 'onion', 'salt']

    # Any food except eggs and daal_items
    rice_people = ['Linkon', 'Rifaz', 'Roman', 'Sabir']
    rice_items = ['rice', 'bhaat', 'fish', 'meat']

    all_people = ['Linkon', 'Rifaz', 'Roman', 'Farzad', 'Sabir', 'Nihal']
    # house_people = all_people.copy.append('Sourav')

    # items_list = {group, people_array}
    items_list = {
            'daal': daal_people,
            'rice': rice_people,
            'egg' : egg_people,
            'any' : all_people
    }

    for row in items:
        [item, price] = row.split()

        if item in daal_items:
            group = 'daal'
        elif item in rice_items:
            group = 'rice'
        elif item == 'egg':
            group = 'egg'
        else:
            group = 'any'

        print('\nItem:', item)
        for consumer_name in items_list[group]:
            num_of_consumers = len(items_list[group])
            price_to_pay = int(price) / num_of_consumers
            consumer = people[consumer_name]
            consumer.current_cash -= price_to_pay
            print(consumer.name+ ' currently has: ' + str(consumer.current_cash))
            #print(consumer.name + ' should pay: ' + str(price_to_pay))
            r.available_cash -= price_to_pay
            
        print('Total consumers:', num_of_consumers)
