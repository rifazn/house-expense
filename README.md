# Description

This is a webapp used to calculate the house expenses for the uiu students living in Bashundhara E15 (wifi name: sudo_die). The calculations can get quite tricky sometimes as there are many preferences for each individual members. This webapp should help in that regard.

## How the calculations are done

### Lets start with an example
The list of people who consume apples are:

    * Rifaz
    * Linkon
    * Sabir
    * Nihal

Assuming each of them paid 50 tk to the reserve, and apples worth 80 tk was bought that week, then 20 tk from each of these people's account in the reserve will be deducted. Leaving each of them with 30 to be carried forward to the next week.

### To sum up

1. We have one guy acting as a reserve in the house. The other guys give him money and he uses that money to buy food. 
2. Not everyone consumes everything, so we maintain a list of everyone's preferences.
3. When an item is bought, the shared cost of the item is deducted from the accounts of all those who consume that item.
4. If the week ends, the amount of money that was left in a housemate's account is brought forward to the next week.
5. A new week starts and the process repeats.


# How to Use

## Basic usage

The index page displays a list of house mates along with their dues for that week to the Reserve. This page is used for doing the advance payment to the Reserve. After advance payment is done, i.e. clicked on the submit button, the current balance of each house mate is shown.

The page 'Scan Items and Evaluate' takes a list of items and their prices. Upon clicking the Submit button, it deducts the correct amount from the accounts of the housemates who consume that item.

## Additional usage

The page 'Who Haven't Paid' displays the list of people and their due amount to be paid to the Resereve.

The page 'Current Accounts' can be used to display the amount of money of each housemate currently left to the Reserve.

The 'config' page (not yet implemented) should allow the user to change the preference of the items consumed of each user.

And that is all!

The site is currently hosted at: https://rifazn.pythonanywhere.com

