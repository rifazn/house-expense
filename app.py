from house_expense import *
from helpers import *

# TODO: 1. Taking advance payments
#       2. Implementing the bazar


def main():
    with shelve.open('he-data') as db:
        try:
            people = db['people']
        except:
            people = init_people()
            print('no people in db')

        try:
            reserve = db['reserve']
        except:
            reserve = Reserve('Linkon')
            reserve.init_payments()
            print('no reserve in db')

    # ask user for input
    user_choice = input(
        "1/a: Take advance payments\n2/b: Scan list of items: "
    )

    if user_choice in '1a':
        name = input("Name of HouseMate, or type 'all': ")
        if name == 'all':
            take_adv_payment_for_all(people, reserve)
        else:
            amount = float(input('Amount: '))
            hm = people[name]
            take_adv_payment(hm, amount, reserve)
    elif user_choice in '2b':
        items_file = open('items_bought.txt', 'r').read().split('\n')

        # sanitization for trailing empty indices
        for _ in range(items_file.count('')):
            items_file.remove('')
        with open('items_bought.txt') as items_file:
            items = items_file.readlines()
        items = list(map(lambda s: s.strip(), items))

        scan_items_and_evaluate(people, items)
    else:
        for person in people.values():
            print(person.name, person.current_cash)

    with shelve.open('he-data') as db:
            db['people'] = people 
            db['reserve'] = reserve

if __name__ == "__main__":
    main()

