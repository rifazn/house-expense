#!/bin/python3

import sys

class HouseMate:
    def __init__(self, name):
        self.name = name
        self.advance_payment = 0.0  # Negative advance payment will mean that
                                    # this HouseMate needs to pay more.

class Reserve(HouseMate):
    def __init__(self, name):
        HouseMate.__init__(self, name)
        self.dues = None
        self.has_cash = 0.0 # Negative cash means, the reserve paid from its
                            # own pocket. The HouseMates must restore it back

nihal = HouseMate('Nihal')
farzad = HouseMate('Farzad')
rifaz = HouseMate('Rifaz')
roman = HouseMate('Roman')
linkon = HouseMate('Linkon')
sabir = HouseMate('Sabir')


# These people pay for eggs in the house
egg_people = [linkon, roman, rifaz, sabir]

# These people pay for daal, oil, onion, garlic and salt
daal_people = [linkon, rifaz, roman, farzad, nihal, sabir]
daal_items = ['daal', 'oil', 'garlic', 'onion', 'salt']

# Any food except eggs and daal_items
rice_people = [linkon, rifaz, roman, sabir]

all_people = [linkon, rifaz, roman, farzad, sabir, nihal]
# house_people = all_people.copy.append('Sourav')

items_list = {
        'daal': daal_people,
        'rice': rice_people,
        'eggs': egg_people,
        'fish': rice_people
}

items_file = open('items_bought.txt', 'r').read().split('\n')

def take_advance():
    # This will later be populated in a more systematic way
    sabir.advance_payment += 200.0
    nihal.advance_payment += 100.0
    farzad.advance_payment += 100.0
    rifaz.advance_payment += 200.0
    roman.advance_payment += 200.0
    linkon.advance_payment += 200.0

    for people in all_people:
        print(people.name, people.advance_payment)

def bazar():
    # sanitize for trailing empty indices
    for _ in range(items_file.count('')):
        items_file.remove('')

    for row in items_file:
        [item, price] = row.split()

        if item in daal_items:
            group = 'daal'

        print('\nItem:', item)
        for consumer in items_list[group]:
            num_of_consumers = len(items_list[group])
            price_to_pay = int(price) / num_of_consumers
            consumer.advance_payment -= price_to_pay
            print(consumer.name + ' currently has: ' + str(consumer.advance_payment))
            #print(consumer.name + ' should pay: ' + str(price_to_pay))
        print('Total consumers:', num_of_consumers)


if __name__ == "__main__":
    arg = sys.argv[1]
    if arg == '-p':
        take_advance()
    elif arg == '-b':
        bazar()
    else:
        print("Usage: house_expense arguments file")
        print('Arguments:')
        print("-p:  Update advance payment")
        print("-b:  bazar")
