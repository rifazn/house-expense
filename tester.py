#!/bin/python3

from house_expense import *

reserve = Reserve('Linkon')

nihal = HouseMate('Nihal')
rifaz = HouseMate('Rifaz')

reserve.set_payments()

print('Payment of the buddies:')
for n, amt in reserve.payment_amt.items():
    print(n, amt)

bud_names = ['Nihal', 'Linkon', 'Roman', 'Farzad', 'Rifaz', 'Sabir']
buds = []
p = [90.00, 230.00, 180.00, 100.00, 200.00, 10.00]
payments_made = []
for names in bud_names:
    buds.append(HouseMate(names))

for bud, amt in zip(buds, p):
    payments_made.append(Payment(bud, amt))

print('All the amounts that will be paid to reserve:')
for p in payments_made:
    print(p.by, p.amt)

# Hope the payments list was populated alright
# Let's try to add some payments to the Reserve now
for p in payments_made:
    reserve.take_payment(p)
# 
reserve.who_are_left()
